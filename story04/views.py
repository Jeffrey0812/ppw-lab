from django.shortcuts import render

# Create your views here.
def index(request):
    return render(request, 'story04/index.html',{})

def about(request):
    return render(request, 'story04/about.html', {})

def achievements(request):
    return render(request, 'story04/achievements.html', {})

def contact(request):
    return render(request, 'story04/contact.html', {})

def education(request):
    return render(request, 'story04/education.html', {})

def experiences(request):
    return render(request, 'story04/experiences.html', {})

def guestbook(request):
    return render(request, 'story04/guest-book.html', {})
