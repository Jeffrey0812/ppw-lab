from django.urls import path
from . import views

urlpatterns = [
    path('',views.index, name='index'),
    path('about/', views.about, name='about'),
    path('achievements/', views.achievements, name='achievements'),
    path('contact/', views.contact, name='contact'),
    path('education/', views.education, name='education'),
    path('experiences/', views.experiences, name='experiences'),
    path('guestbook/', views.guestbook, name='guestbook'),

]
